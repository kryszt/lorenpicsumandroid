package org.kryszt.lorenpicsum

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.android.viewmodel.ext.android.viewModel
import org.kryszt.lorenpicsum.states.PictureInfo
import org.kryszt.lorenpicsum.ui.details.PictureDetailsFragment
import org.kryszt.lorenpicsum.ui.list.PictureListFragment
import org.kryszt.lorenpicsum.viewmodel.PicsumListViewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel: PicsumListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, PictureListFragment.newInstance())
                .commitNow()
        }
        initObservers()
    }

    private fun initObservers() {
        mainViewModel.detailsSelected.observe(
            this,
            Observer { it?.executeIfNotConsumed(this::showDetails) })
    }

    private fun showDetails(picture: PictureInfo) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, PictureDetailsFragment.newInstance(picture))
            .addToBackStack(PictureDetailsFragment.TAG)
            .commit()
    }

}
