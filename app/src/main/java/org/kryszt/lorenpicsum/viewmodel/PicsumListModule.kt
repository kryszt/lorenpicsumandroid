package org.kryszt.lorenpicsum.viewmodel

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val picsumListModule = module {
    viewModel { PicsumListViewModel(get()) }
}
