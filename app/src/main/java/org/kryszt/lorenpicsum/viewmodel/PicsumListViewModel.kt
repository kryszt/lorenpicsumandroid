package org.kryszt.lorenpicsum.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.kryszt.lorenpicsum.service.PicsumItemInfo
import org.kryszt.lorenpicsum.service.PicsumService
import org.kryszt.lorenpicsum.states.ListState
import org.kryszt.lorenpicsum.states.PictureInfo
import org.kryszt.lorenpicsum.util.ConsumableItem

class PicsumListViewModel(private val service: PicsumService) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val paginationHelper = PaginationHelper()

    private val _detailsSelected = MutableLiveData<ConsumableItem<PictureInfo>>()
    val detailsSelected: LiveData<ConsumableItem<PictureInfo>>
        get() = _detailsSelected

    private val _listState = MutableLiveData<ListState>(ListState.InitialLoading)
    val listState: LiveData<ListState> = _listState

    init {
        service
            .pictureList(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map<ListState> { it.toReadyState() }
            .onErrorReturn { ListState.InitialLoadingError(it.localizedMessage) }
            .subscribe(_listState::setValue)
            .also { disposables.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    fun pictureClicked(picture: PictureInfo) {
        _detailsSelected.value = ConsumableItem(picture)
    }

    fun positionDisplayed(position: Int) {
        val state = _listState.value ?: return
        when (val result = paginationHelper.shouldLoadNextPage(state, position)) {
            is PaginationHelper.CheckResult.LoadPage -> loadNextPage(result.nextPage)
        }
    }

    private fun loadNextPage(nextPage: Int) {
        val state = _listState.value ?: return
        if (state is ListState.Ready) {
            val newState = state.copy(isLoadingMore = true)
            _listState.value = newState

            //FIXME -> merging state with call result should be extracted to separate class
            service.pictureList(nextPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.map(PicsumItemInfo::toPictureInfo) }
                .map<ListState> { newItems ->
                    state.copy(
                        isLoadingMore = false,
                        errorMessage = null,
                        pictures = state.pictures + newItems
                    )
                }
                .onErrorReturn { state.copy(isLoadingMore = false, errorMessage = it.message) }
                .subscribe(_listState::setValue)
                .also { disposables.add(it) }
        }
    }

}

private fun List<PicsumItemInfo>.toReadyState(): ListState.Ready =
    ListState.Ready(this.map { it.toPictureInfo() })

private fun PicsumItemInfo.toPictureInfo(): PictureInfo = PictureInfo(
    id = this.id,
    author = this.author,
    listImageUrl = "https://picsum.photos/id/${this.id}/200/200",//TODO -> use configurable size instead of hardcoded. A class building urls would be nice
    fullImageUrl = this.download_url,
    shareUrl = this.url
)
