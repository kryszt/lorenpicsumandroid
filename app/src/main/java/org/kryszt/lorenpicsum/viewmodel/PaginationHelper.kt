package org.kryszt.lorenpicsum.viewmodel

import org.kryszt.lorenpicsum.states.ListState

class PaginationHelper {

    //TODO: these fields could be constructor parameters, but with only one source of data it is pointless now
    private val nextPageThreshold = 10
    private val pageSize = 30

    fun shouldLoadNextPage(state: ListState, boundPosition: Int): CheckResult {
        return if (state is ListState.Ready
            && !state.isLoadingMore
            && inLoadThreshold(state.pictures.size, boundPosition)
        ) {
            CheckResult.LoadPage(nextPageIndex(state.pictures.size))
        } else {
            CheckResult.DoNotLoad
        }
    }

    private fun inLoadThreshold(size: Int, boundPosition: Int): Boolean {
        return size - boundPosition < nextPageThreshold
    }

    private fun nextPageIndex(size: Int): Int {
        return size / pageSize + 1
    }

    sealed class CheckResult {
        object DoNotLoad : CheckResult()
        data class LoadPage(val nextPage: Int) : CheckResult()
    }
}
