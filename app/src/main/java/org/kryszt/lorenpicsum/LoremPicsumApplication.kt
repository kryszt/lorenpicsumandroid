package org.kryszt.lorenpicsum

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.kryszt.lorenpicsum.di.networkModule
import org.kryszt.lorenpicsum.viewmodel.picsumListModule

class LoremPicsumApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@LoremPicsumApplication)
            modules(listOf(networkModule, picsumListModule))
        }
    }
}
