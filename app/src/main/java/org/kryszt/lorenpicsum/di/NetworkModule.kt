package org.kryszt.lorenpicsum.di

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import org.kryszt.lorenpicsum.BuildConfig
import org.kryszt.lorenpicsum.service.PicsumService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .let {
                if (BuildConfig.DEBUG) {
                    it.withDebug()
                } else {
                    it
                }
            }
            .build()
    }

    single<Retrofit> {
        Retrofit.Builder()
            .client(get())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://picsum.photos/")
            .build()
    }

    single<PicsumService> {
        get<Retrofit>().create(PicsumService::class.java)
    }
}

private fun OkHttpClient.Builder.withDebug(): OkHttpClient.Builder =
    addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
