package org.kryszt.lorenpicsum.states

sealed class ListState {
    object InitialLoading : ListState()
    class InitialLoadingError(val errorMessage: String) : ListState()
    data class Ready(
        val pictures: List<PictureInfo> = emptyList(),
        val errorMessage: String? = null,
        val isLoadingMore: Boolean = false
    ) : ListState()
}
