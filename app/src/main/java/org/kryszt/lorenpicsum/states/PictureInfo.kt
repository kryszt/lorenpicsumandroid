package org.kryszt.lorenpicsum.states

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PictureInfo(
    val id: String,
    val author: String,
    val listImageUrl: String,
    val fullImageUrl: String,
    val shareUrl: String
) : Parcelable
