package org.kryszt.lorenpicsum.service

data class PicsumItemInfo(
    val id: String,
    val author: String,
    val width: Int,
    val height: Int,
    val url: String,
    val download_url: String
)
