package org.kryszt.lorenpicsum.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PicsumService {

    @GET("/v2/list")
    fun pictureList(@Query("page") page: Int): Single<List<PicsumItemInfo>>
}
