package org.kryszt.lorenpicsum.util

class ConsumableItem<T>(private val item: T) {
    private var isConsumed: Boolean = false

    fun executeIfNotConsumed(action: (T) -> Unit) {
        if (!isConsumed) {
            isConsumed = true
            action(item)
        }
    }
}
