package org.kryszt.lorenpicsum.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.kryszt.lorenpicsum.R
import org.kryszt.lorenpicsum.states.PictureInfo

class PictureListAdapter(private val onItemClick: (PictureInfo) -> Unit) :
    ListAdapter<PictureInfo, PictureListItemVH>(PictureInfoDiffer) {

    var bindingPositionListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureListItemVH {
        return PictureListItemVH(
            LayoutInflater.from(parent.context).inflate(R.layout.picture_list_item, parent, false),
            ::positionClicked
        )
    }

    private fun positionClicked(position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            getItem(position)?.let(onItemClick)
        }
    }

    override fun onBindViewHolder(holder: PictureListItemVH, position: Int) {
        val picture = getItem(position)
        Picasso.get().load(picture.listImageUrl).into(holder.imageView)
        bindingPositionListener?.invoke(position)
    }
}

private object PictureInfoDiffer : DiffUtil.ItemCallback<PictureInfo>() {

    override fun areItemsTheSame(oldItem: PictureInfo, newItem: PictureInfo): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PictureInfo, newItem: PictureInfo): Boolean =
        oldItem == newItem
}
