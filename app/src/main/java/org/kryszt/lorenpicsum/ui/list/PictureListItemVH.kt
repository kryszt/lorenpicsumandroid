package org.kryszt.lorenpicsum.ui.list

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import org.kryszt.lorenpicsum.R

class PictureListItemVH(view: View, onClicked: (Int) -> Unit) : RecyclerView.ViewHolder(view) {

    init {
        view.setOnClickListener {
            onClicked(adapterPosition)
        }
    }

    val imageView: ImageView = view.findViewById(R.id.itemImage)

}
