package org.kryszt.lorenpicsum.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.kryszt.lorenpicsum.R
import org.kryszt.lorenpicsum.setVisibleOrGone
import org.kryszt.lorenpicsum.states.ListState
import org.kryszt.lorenpicsum.viewmodel.PicsumListViewModel

class PictureListFragment : Fragment() {

    private lateinit var picturesAdapter: PictureListAdapter

    private val pictureListViewModel: PicsumListViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initGrid()


        connectObservers()
    }

    private fun initGrid() {
        picturesAdapter = PictureListAdapter { pictureListViewModel.pictureClicked(it) }
        picturesAdapter.bindingPositionListener = { pictureListViewModel.positionDisplayed(it) }
        picturesGrid.adapter = picturesAdapter
        // would be good to add decorator with some space between items
    }

    private fun connectObservers() {
        pictureListViewModel.listState.observe(
            viewLifecycleOwner,
            Observer { it?.let(::showState) })
    }

    private fun showState(state: ListState) {
        updateSectionsVisibilities(state)

        when (state) {
            is ListState.InitialLoadingError -> mainErrorIndicator.text = state.errorMessage
            is ListState.Ready -> picturesAdapter.submitList(state.pictures)
        }
    }

    private fun updateSectionsVisibilities(state: ListState) {
        picturesGrid.setVisibleOrGone(state is ListState.Ready)
        mainErrorIndicator.setVisibleOrGone(state is ListState.InitialLoadingError)
        mainLoadingIndicator.setVisibleOrGone(state is ListState.InitialLoading)
    }

    companion object {
        fun newInstance() = PictureListFragment()
    }

}
