package org.kryszt.lorenpicsum.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.picture_details_fragment.*
import org.kryszt.lorenpicsum.R
import org.kryszt.lorenpicsum.states.PictureInfo

class PictureDetailsFragment : Fragment() {

    private val picture: PictureInfo
        get() = arguments!!.getParcelable(KEY_PICTURE)!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.picture_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        share.setOnClickListener { sharePicture() }
        showPicture()
    }

    private fun showPicture() {
        Picasso.get().load(picture.fullImageUrl).into(detailsPicture)
        authorTextView.text = picture.author
    }

    private fun sharePicture() {
        ShareCompat.IntentBuilder.from(requireActivity())
            .setType("text/plain")
            .setChooserTitle(R.string.share_dialog_title)
            .setText(picture.shareUrl)
            .startChooser()
    }

    companion object {

        const val TAG = "PictureDetailsFragment"
        private const val KEY_PICTURE = "key.picture"

        fun newInstance(picture: PictureInfo): PictureDetailsFragment {
            return PictureDetailsFragment()
                .apply {
                    arguments = Bundle().apply {
                        putParcelable(KEY_PICTURE, picture)
                    }
                }
        }
    }
}
