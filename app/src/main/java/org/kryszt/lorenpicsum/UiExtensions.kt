package org.kryszt.lorenpicsum

import android.view.View

fun View.setVisibleOrGone(visible: Boolean) {
    when (visible) {
        true -> setVisible()
        else -> setGone()
    }
}

fun View.setGone() {
    visibility = View.GONE
}

fun View.setVisible() {
    visibility = View.VISIBLE
}

