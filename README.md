Used libraries:
- Architecture components (ViewModels, LiveData)
- Retrofit to create API interface
- Picasso for loading pictures
- RxJava for handling retrofit calls and easier error handling
- Koin for dependency injection

Basic Architecture:
- Single activity with two fragments (list and detail)
- MVVM with ViewModel shared between activity and list fragment
- Detail fragment receives picture to show as argument  

Design decisions:
- List data held as state. Separate states for initial loading and error, as those are edge cases.
- Ready state with information on ongoing loading more and errors on loading more
- Clicks on items are passed through ViewModel. This allows ListFragment to be independent from activity. 
- ConsumableItem created to prevent handling of clicks multiple times (i.e. on screen rotation)

Possible implementation improvements:
- List state should have information on next page number. It could be done i.e. by wrapping PicsumService in a use case that would add such information to state
- UseCase would also allow to change retrofit service to any other source without changing ViewModel

Features that should be included:
- Pool-to-refresh (quite easy, but I had limited time)
- CoordinatorLayout on list, to hide toolbar. I didn't want to spend time searching which google library provides it
- Properly setup caches for Picasso and OkHttp

No tests
- Unfortunately, creating tests for ViewModels that use RxJava and LiveData is something that takes a lot of time for me. Setting up all the threading rules to make it work would take me a lot of time.
- The same goes for testing UI. Although I did write them, I use old tools and predefined test configuration. I didn't feel that spending time on that would help me here
